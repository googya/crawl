namespace :study do
   desc "generate faker data"
   task :gen_data => :environment  do 
      300.times do
         study = Study.new
         study.title = Faker::Lorem.sentence
         study.url = Faker::Internet.url
         study.content = Faker::Lorem.paragraph
         study.identifier = Faker::Code.ean
         study.save
      end
   end
end
