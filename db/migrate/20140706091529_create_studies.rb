class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.string :title
      t.text :content
      t.string :identifier
      t.string :url

      t.timestamps
    end
  end
end
