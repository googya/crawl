class StudiesController < ApplicationController
   def index
      page = params[:page] || 1
      per = params[:per] || 20
      @studies = Study.page(page).per(per)
   end
end
